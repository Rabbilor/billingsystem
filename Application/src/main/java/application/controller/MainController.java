package application.controller;

import application.entity.Customer;
import application.service.BillingService;
import application.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class MainController {
   
    @Autowired
    BillingService billingService;
    @Autowired
    CustomerService customerService;

    @GetMapping("/user/me")
    public Principal user(Principal principal) {
        return principal;
    }

    @RequestMapping(value = "/billing", method = RequestMethod.GET)
    public String getGreetingFromBE() {
        return billingService.getServiseName();
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public Collection<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    private String addCustomers() {
        List<Customer> customers = new ArrayList<Customer>() {{
            add(new Customer("alex", "password", "Alex", "Lion", 191L));
            add(new Customer("tommy", "$2a$04$CbGkvVqqINcsJkmvBeT9wOPB4ABGEgaEiLibci/CsevqQNQ3ySZ6G", "Tommy", "Cash", 292L));
            add(new Customer("timmy", "$$2a$04$CbGkvVqqINcsJkmvBeT9wOPB4ABGEgaEiLibci/CsevqQNQ3ySZ6G", "Timmy", "Terner", 911L));
            add(new Customer("mike", "$2a$04$CbGkvVqqINcsJkmvBeT9wOPB4ABGEgaEiLibci/CsevqQNQ3ySZ6G", "Mike", "Vazovski", 112L));
        }};
        for (Customer i : customers) {
            customerService.save(i);
        }
        return "added";
    }
}