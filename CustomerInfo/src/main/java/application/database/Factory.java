package application.database;


public class Factory {


    private static CustomerDAO customerDAO = null;
    private static Factory instance = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public CustomerDAO getCustomerDAO(){
        if (customerDAO == null){
            customerDAO = new CustomerDAOImpl();
        }
        return customerDAO;
    }

}
