package application.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String getGreetingFromCustomer(@RequestParam(name = "hello",required = false,defaultValue = "Customer") String msg) {
        return "Hello from " + msg + "!";
    }

}
