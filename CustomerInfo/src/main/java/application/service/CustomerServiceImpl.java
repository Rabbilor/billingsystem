package application.service;

import application.database.Factory;
import application.entity.Customer;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService, UserDetailsService {

    @Override
    public Collection<Customer> getAllCustomers() {
        try {
            return Factory.getInstance().getCustomerDAO().getAllCustomers();
        } catch (SQLException exc) {
            return null;
        }
    }

    @Override
    public void save(Customer customer) {
        try {
            Factory.getInstance().getCustomerDAO().addCustomer(customer);
        } catch (SQLException exc) {
            throw new RuntimeException(exc.getMessage(), exc);
        }
    }

    private List getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ADMIN"));
    }

    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {

        Customer customer = null;
        try {
            customer = Factory.getInstance().getCustomerDAO().getCustomerByUsername(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (customer == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(customer.getUsername(), customer.getPassword(), getAuthority());
    }
}
