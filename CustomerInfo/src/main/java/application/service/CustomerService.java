package application.service;

import application.entity.Customer;

import java.util.Collection;

public interface CustomerService {

    Collection<Customer> getAllCustomers();

    void save(Customer customer);
}
